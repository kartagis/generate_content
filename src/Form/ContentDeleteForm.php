<?php

namespace Drupal\generate_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;


class ContentDeleteForm extends FormBase {

  public function getFormId() {
    return 'content_delete_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $nodes = \Drupal::entityQuery('node')->execute();
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete @num nodes', ['@num' => count($nodes)]),
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Deleting nodes'),
      'init_message' => $this->t('Processing'),
      'operations' => [],
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred while processing'),
      'finished' => '\Drupal\generate_content\Controller\ContentDeleteController::BatchFinished',
    ];
    $nodes = Node::loadMultiple();
    foreach ($nodes as $node) {
      $batch['operations'][] = ['\Drupal\generate_content\Controller\ContentDeleteController::DeleteContent', [$node]];
    }
    batch_set($batch);
  }
}