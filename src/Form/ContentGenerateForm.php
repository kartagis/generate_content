<?php

namespace Drupal\generate_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class ContentGenerateForm extends FormBase {

  public function getFormId() {
    return 'content_generate_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['num_of_nodes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Please specify how many nodes to generate.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate'),
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $num = $form_state->getValue('num_of_nodes');
    $batch = [
      'title' => $this->t('Generating nodes'),
      'init_message' => $this->t('Processing'),
      'operations' => [],
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred while processing'),
      'finished' => '\Drupal\generate_content\Controller\ContentGenerateController::BatchFinished',
    ];
    for($i = 0;$i < $num;$i++) {
      $content = [];
      $batch['operations'][] = ['\Drupal\generate_content\Controller\ContentGenerateController::GenerateContent', [$content]];
    }
    batch_set($batch);
  }
}