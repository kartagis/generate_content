<?php

namespace Drupal\generate_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class ContentDeleteController extends ControllerBase {

  public static function DeleteContent($node) {
    $node->delete();
  }

  public static function BatchFinished() {
    \Drupal::messenger()->addMessage(t('Finished deleting nodes.'));
  }
}