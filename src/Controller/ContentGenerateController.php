<?php

namespace Drupal\generate_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Component\Datetime\Time;

class ContentGenerateController extends ControllerBase {

  public static function GenerateContent($content) {
    $node = Node::create([
      'type' => 'page',
      'uid' => 1,
      'title' => rand(0,5000),
      'body' => [
        'summary' => '',
        'value' => rand(0,5000),
        'format' => 'full_html',
      ],
    ]);
    $node->save();
  }

  public static function BatchFinished() {
    \Drupal::messenger()->addMessage(t('Finished creating nodes.'));
  }
}